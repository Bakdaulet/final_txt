package hello;

import com.oracle.tools.packager.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.soap.Text;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

/**
 * Created by BahaWood on 13/05/17.
 */
@Controller
@RequestMapping(value = "final")
public class MainController {
    @Autowired
    TextRepo textRepo;

    private static String UPLOAD_DIR = "/Users/tehno/Documents/FinalTask/src/main/upload/";
    private static final DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    Date date = new Date();

    @RequestMapping(value = "txtFile", method = RequestMethod.PUT)
    @ResponseBody
    public String txtUpload(@RequestParam("text") MultipartFile file) throws IOException{

        String format = file.getOriginalFilename();
        String formatFile = format.substring(format.length()-3);
        ModelText modelTxt = new ModelText();
        String for_return = "sdsd";

        if(formatFile.equals("txt")) {
            byte[] bytes = file.getBytes();
            String completeData = new String(bytes);
            String rows[] = completeData.split(" ");
            String name = UUID.randomUUID().toString();

                if (completeData.contains("spam")) {
                    File file_spam = new File(UPLOAD_DIR + "spam");
                    if(!file_spam.exists()){
                        file_spam.mkdir();
                    }
                    Path path = Paths.get(UPLOAD_DIR + "spam/" + name + ".txt");
                    Files.write(path, bytes);

                    modelTxt.setName(name);
                    modelTxt.setPath(path.toString());
                    modelTxt.setDate(date);
                    textRepo.save(modelTxt);

                    return "Spam";

                } else {
                    File file_no_spam = new File(UPLOAD_DIR + sdf.format(date));
                    if(!file_no_spam.exists()){
                        file_no_spam.mkdir();
                    }
                    Path path = Paths.get(UPLOAD_DIR + sdf.format(date) + "/" + name + ".txt");
                    Files.write(path, bytes);

                    modelTxt.setName(format);
                    modelTxt.setPath(path.toString());
                    modelTxt.setDate(date);
                    textRepo.save(modelTxt);

                    return  "Without Spam";
                }
        } else {
            return "error type of file";
        }
    }
    @RequestMapping(value = "delete", method = RequestMethod.DELETE)
    @ResponseBody
    public String deleteSpam(){

        Iterable<ModelText> all = textRepo.findAll();

        for(ModelText f : all){
            File files = new File(f.getPath());
            String path = files.getParent();
            if(path.substring(path.lastIndexOf("/")+1, path.length()).equals("spam")){
                textRepo.delete(f);
                File fileInLocal = new File(f.getPath());
                if(fileInLocal.exists() && !fileInLocal.isDirectory()) {
                    fileInLocal.delete();
                }
            }
        }

        return "deleted";
    }

}
